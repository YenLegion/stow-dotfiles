hi clear
syntax reset
set background=dark
let colors_name = "monochrome"

hi Boolean ctermfg=15
hi Character ctermfg=15
hi ColorColumn ctermbg=233
hi Comment ctermfg=8
hi Conceal ctermfg=8 ctermbg=0
hi Conditional ctermfg=15
hi Constant ctermfg=15
hi CursorColumn ctermbg=242
hi Cursor ctermfg=15 ctermbg=238
hi CursorLine cterm=none ctermbg=none
hi CursorLineNr ctermfg=7 cterm=none
hi DiffAdd ctermfg=15 ctermbg=234
hi DiffChange ctermbg=234 ctermfg=15
hi DiffDelete ctermbg=16 ctermfg=15
hi DiffText ctermfg=15 ctermbg=234
hi Directory ctermfg=15
hi Error ctermfg=15 ctermbg=234
hi FoldColumn ctermbg=none ctermfg=15
hi Folded ctermbg=0 ctermfg=8
hi Function ctermfg=15
hi Identifier ctermfg=15 cterm=none
hi Ignore ctermfg=0
hi IncSearch cterm=reverse
hi Keyword ctermfg=15
hi LineNr ctermfg=8
hi MatchParen ctermbg=0 ctermfg=15
hi ModeMsg cterm=bold
hi MoreMsg ctermfg=15
hi NonText ctermfg=8
hi Normal ctermfg=15 ctermbg=0
hi Number ctermfg=15
hi Operator ctermfg=15
hi Pmenu ctermfg=15 ctermbg=0
hi PmenuSbar ctermbg=0
hi PmenuSbar ctermbg=15
hi PmenuSel ctermbg=15 ctermfg=0
hi PmenuThumb ctermbg=15
hi PreProc ctermfg=15
hi Question ctermfg=15
hi Search ctermbg=15 ctermfg=0
hi SignColumn ctermfg=15 ctermbg=none
hi Special ctermfg=15
hi SpecialKey ctermfg=15
hi SpellBad ctermbg=52 ctermfg=15
hi SpellCap ctermfg=15 ctermbg=23
hi SpellLocal ctermbg=25
hi SpellRare ctermbg=130
hi Statement ctermfg=15
hi StatusLine ctermfg=15 ctermbg=0 cterm=bold
hi StatusLineNC ctermbg=15 ctermfg=0
hi StatusLineTerm cterm=bold ctermfg=15 ctermbg=0
hi StatusLineTermNC ctermfg=15 ctermbg=0 cterm=none
hi String ctermfg=15
hi TabLine ctermfg=15 ctermbg=0
hi TabLineFill ctermfg=15 ctermbg=0
hi TabLineSel ctermfg=15 ctermbg=0
hi Title ctermfg=15
hi Todo ctermfg=15 ctermbg=0 cterm=bold
hi ToolbarButton ctermfg=0 ctermbg=15
hi ToolbarLine ctermbg=0
hi Type ctermfg=15
hi Underlined ctermfg=15
hi VertSplit ctermfg=0 ctermbg=8
hi Visual ctermbg=240
hi Visual ctermfg=0 ctermbg=15
hi WarningMsg ctermfg=224
hi WildMenu ctermfg=15 ctermbg=0
